@extends('layouts.master')

@section('content')
    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Masters</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    
                </ul>
            </div>
        </div>

        <div class="panel-body">
            
        </div>

        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>S.no</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(!count($collection))

                @else
                    @foreach($collection as $index => $model)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $model->title }}</td>
                            <td>{{ $model->slug }}</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>    
@stop

@section('script')
    <script>
        $(document).ready(function() {
            $('.master-menu').addClass('active');
        });
    </script>
@stop