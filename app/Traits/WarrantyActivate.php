<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Product;
use App\Models\UserWarranty;
use App\Models\UserWarrantyDetail;
use Illuminate\Support\Facades\Log;
use App\Models\BrandsProductAttribute;

trait WarrantyActivate
{
    protected function getEmployeeLocationId($user, $permission)
    {

        Log::alert($permission);

        return $user
                    ->permissions
                    ->where('slug', $permission)
                    ->pluck('pivot')
                    ->pluck('employer_location_id')
                    ->toArray();
    }

    protected function getDealerLocationId($user, $permission)
    {
        Log::alert($permission);

        return $user
                    ->permissions
                    ->where('slug', $permission)
                    ->pluck('pivot')
                    ->pluck('employer_location_id')
                    ->toArray();
    }

    protected function getOrderId($user, $dealer_location_ids, $employee_location_ids)
    {
        $dealer_order_ids = Order::whereIn('dealer_location_id', $dealer_location_ids)
                                ->get()
                                ->pluck('id')
                                ->toArray();

        $employees_order_ids = Order::whereIn('dealer_location_id', $employee_location_ids)
                                ->where('created_by', $user->id)
                                ->get()
                                ->pluck('id')
                                ->toArray();

        return array_merge($dealer_order_ids, $employees_order_ids);
    }

    protected function ActivateWarranty($order)
    {
        $response=[];
        if( ! $order ){
            $response['status'] = 0;
            $response['message'] = 'Order not found or already activated!';
            return $response ;
        }   else    {
                /**
                 * Document validation
                 */
                $documents = $order->documents;

                if( $documents->count() == 0 || $documents->where('type', 'invoices')->count() == 0 )  {
                    $response['status'] = 0;
                    $response['message'] = 'Invoice document is required to activate this order.' ;
                    return $response ;
                }

                /**
                 * Purchase Date validation
                 */
                // $total_days = Carbon::today()->diffInDays( $order->purchase_date );

                if($order->purchase_date->toDateString() > Carbon::now()->toDateString() ) {
                    $response['status'] = 0;
                    $response['message'] = 'Purchase date is invalid.' ;
                    return $response ;
                }

                /**
                 * Transaction address validation
                 */
                if( is_null( $order->transaction_address_id ) )   {

                    $response['status'] = 0;
                    $response['message'] ='Consumer address is invalid.';
                    return $response ;
                }

                /**
                 * Brand, Category & Product validation
                 */
                if( is_null( $order->detail->product_id ) )   {
                    $response['status'] = 0;
                    $response['message'] ='Product is invalid.' ;
                    return $response ;
                }   else if (
                    $order->detail->brand_id !== $order->detail->product->brand_id ||
                    $order->detail->category_id !== $order->detail->product->categories->first()->id
                )   {

                    $response['status'] = 0;
                    $response['message'] ='Product is invalid.';
                    return $response ;
                }

                $model = $order->detail->product_id;
                $product = Product::active()->whereId($model)->first();
                $warranty = $product->default_warranty->first();
                $order_detail = $order->detail;

                if( ! $warranty ) {
                    $response['status'] = 0;
                    $response['message'] ='We are in process of on-boarding this product.' ;
                    return $response ;
                }

                // TODO: Add/Update address
                // TODO: Add/Update transaction address
                // TODO: Update order

                $order->update([
                    'status'    => 'Approved',
                ]);

                // TODO: Update order-detail
                // TODO: Add/Update documents

                /**
                 * Product attribute instertion
                 */
                $attributes = BrandsProductAttribute::where('warranty_product_id', $warranty->id)->get();
                $end_at = $order->purchase_date;

                if( optional($order->detail)->warranty_in_months > 0 )  {

                    $end_at->addMonths(optional($order->detail)->warranty_in_months);
                }

                $user_warranty = UserWarranty::where('order_detail_id', $order->detail->id)
                                                ->first();

                if( ! $user_warranty )  {

                    $user_warranty = UserWarranty::create([
                        'order_detail_id'   =>  $order->detail->id,
                        'status'            =>  'Approved',
                        ]);
                }

                $user_warranty->start_at    =   $order->purchase_date;
                $user_warranty->end_at      =   $end_at;
                $user_warranty->update();

                foreach ($attributes as $attribute) {
                    UserWarrantyDetail::create([
                        'user_warranty_id'      =>  $user_warranty->id,
                        'warranty_product_id'   =>  $warranty->id,
                        'name'                  =>  $attribute->name,
                        'value'                 =>  '',
                        'send_to_brand'         =>  $attribute->send_to_brand,
                        'data_from_brand'       =>  $attribute->data_from_brand,
                    ]);
                }

                $response['status'] = 1;
                $response['message'] = 'Product activated successfully';

            return $response ;
        }
    }
}
