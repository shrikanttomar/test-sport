<?php

namespace App\Traits;

use Exception;
use Softon\Sms\Facades\Sms;
use Illuminate\Support\Facades\Log;

trait SMSService
{
    public function SendSMS($mobile, $message)
    {
        try {

            if( $mobile )    {

                // Log::critical("Sending SMS to {$mobile} with message {$message}");

                if( config('one.mobile.service') )  {

                    $response['message'] = Sms::send($mobile, $message);

                    //Log::alert( print_r($response, 1) );

                    if( is_object($response['message']) )  {

                        $response['status']     =   300;
                        $response['message']    =   print_r( $response, 1 );
                    }   else    {

                        $response['status']     =   200;
                        $response['message']    =   $message;
                    }
                }
            }
        }   catch(Exception $e) {

            // Log::error( $e->getMessage() );
            $response['status']     =   300;
            $response['message']    =   "Something bad happen!";
        }

        return $response;
    }
}
