<?php

namespace App\Traits;

use App\Models\Address;
use App\Models\UserAddress;

trait TransactionUserAddress
{
    protected function createUserAddress(Address $address, $transaction_type)
    {
        $address_data   =   [
            'user_id'           =>  $address->user_id,
            'address_id'        =>  $address->id,
            'pincode_id'        =>  $address->pincode_id,
            'area_id'           =>  $address->area_id,
            'alias'             =>  $address->alias,
            'contact_name'      =>  $address->contact_name,
            'contact_mobile'    =>  $address->contact_mobile,
            'house_no'          =>  $address->house_no,
            'address_1'         =>  $address->address_1,
            'address_2'         =>  $address->address_2,
            'landmark'          =>  $address->landmark,
            'gstno'             =>  $address->gstno,
            'user_type'         =>  $address->user_type,
            'transaction_type'  =>  $transaction_type,
        ];
        $order_address  =   new UserAddress($address_data);
        $order_address->save();

        return $order_address;
    }


    protected function updateUserAddress($customer_address_id, $transaction_id)
    {

        $address = Address::active()
        ->where('id', $customer_address_id)
        ->first();

        $address_data   =   [
            'user_id'           =>  $address->user_id,
            'address_id'        =>  $address->id,
            'pincode_id'        =>  $address->pincode_id,
            'area_id'           =>  $address->area_id,
            'alias'             =>  $address->alias,
            'contact_name'      =>  $address->contact_name,
            'house_no'          =>  $address->house_no,
            'address_1'         =>  $address->address_1,
            'address_2'         =>  $address->address_2,
            'landmark'          =>  $address->landmark,
            'gstno'             =>  $address->gstno,
            'user_type'         =>  $address->user_type,
        ];

        $order_address  = UserAddress::where('id', $transaction_id)
        ->update($address_data);

        return $order_address;
    }
}
