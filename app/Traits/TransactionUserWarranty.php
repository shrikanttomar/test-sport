<?php

namespace App\Traits;

use App\Models\Order;
use App\Models\Product;
use App\Models\UserWarranty;
use App\Models\SalesOrderDetail;
use App\Models\UserWarrantyDetail;
use App\Models\BrandsProductAttribute;

use Illuminate\Support\Facades\Log;

trait TransactionUserWarranty
{
    protected function createUserWarranty(Product $product, Order $order, SalesOrderDetail $salesOrderDetail, $status = 'NEW')
    {
        /**
         * Product attribute instertion
         */
        $attributes = BrandsProductAttribute::where('warranty_product_id', $product->id)->get();

        $user_warranty = UserWarranty::create([
            'order_detail_id'       =>  $order->detail->id,
            'sales_order_detail_id' =>  $salesOrderDetail->id,
            'status'                =>  $status
            ]);

        // Log::info("User warranty created!");

        foreach ($attributes as $attribute) {

            UserWarrantyDetail::create([
                'user_warranty_id'      =>  $user_warranty->id,
                'warranty_product_id'   =>  $product->id,
                'name'                  =>  $attribute->name,
                'value'                 =>  '',
                'send_to_brand'         =>  $attribute->send_to_brand,
                'data_from_brand'       =>  $attribute->data_from_brand,
                ]);
        }

        // Log::info("User warranty attributes created!");

        return true;
    }
}
