<?php
namespace App\Traits;

use App\Models\Role;
use App\Models\Permission;

trait HasPermissions
{
    public function givePermissionsTo(... $permissions)
    {
        $permissions = $this->getAllPermissions($permissions);

        if($permissions === null) {
            return $this;
        }

        $this->permissions()->saveMany($permissions);

        return $this;
    }

    public function deletePermissions( ... $permissions )
    {
        $permissions = $this->getAllPermissions($permissions);

        $this->permissions()->detach($permissions);

        return $this;
    }

    public function refreshPermissions( ... $permissions )
    {
        $this->permissions()->detach();

        return $this->givePermissionsTo($permissions);
    }

    public function hasPermissionTo($permission)
    {
        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }

    public function hasPermissionThroughRole($permission)
    {
        foreach ($permission->roles as $role) {
            if($this->roles->contain($role))
            {
                return true;
            }
        }

        return false;
    }

    public function hasPermission($permission)
    {
        if( is_object($permission) ) {

            return $this->permissions->where('active', true)->contains('slug', $permission->slug);
        } elseif (is_array($permission)) {

            $result = false;
            foreach ($permission as $key => $value) {

                $result = $this->permissions->where('active', true)->contains('slug', $value);
            }
            return $result;
        }else {

            return $this->permissions->where('active', true)->contains('slug', $permission);
        }
    }

    protected function getAllPermissions(array $permissions)
    {
		return Permission::whereIn('slug',$permissions)->get();
    }

    /**
     * A user may be given multiple permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
    * The roles that belong to the user.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
