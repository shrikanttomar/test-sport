<?php

namespace App\Traits;

trait hasModelRequired
{
    /**
     * check address required on the basis of brand,
     * category and product configuration
     *
     * @param App\\Models\\Brand $brand
     * @param App\\Models\\Category $category
     * @param App\\Models\\Product $product
     *
     * @return Boolean
     */
    protected function getModelRequired($brand, $category)
    {
        $required = false;

        if( $brand && $category )   {

            $bc = BrandCategory::where([
                    'brand_id'      =>  $brand->id,
                    'category_id'   =>  $category->id,
                ])->first();

            $required = $bc->is_model_required;
        }

        return $required;
    }
}
