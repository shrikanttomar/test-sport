<?php

namespace App\Traits;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

trait Cover
{
    /**
     * Get the attachment image.
     *
     * @param string $value
     * @return string
     */
    public function getAttachmentAttribute($value)
    {
        return $this->getCoverAttribute($value);
    }

    /**
     * Get the cover image.
     *
     * @param string $value
     * @return string
     */
    public function getCoverAttribute($value)
    {
        $cover_url = asset(config('one.cover.default'));

        if ( env('APP_ENV') === 'local' ) {

            if(Storage::disk('public')->exists( $value ))
                $cover_url = asset("storage/$value");
        }   else    {

            $cover_url = Cache::remember($value, 5*60, function() use($value, &$message) {

                // if(Storage::disk('s3')->exists( $value ))
                    return $this->asset_cdn($value);
                // else
                    // return asset(config('one.cover.default'));
            });
        }

        return $cover_url;
    }

    protected function asset_cdn($value, $aws_bucket = null, $aws_default_region = 'ap-south-1')
    {
        $aws_bucket = $aws_bucket ?? $this->getdefaultBucket();
        $aws_default_region = $aws_default_region ?? env('AWS_DEFAULT_REGION');

        return 'https://' . $aws_bucket . '.s3.' . $aws_default_region . '.amazonaws.com/' . $value;
    }

    protected function upload($request, $type, $element = 'cover', $location = 's3')
    {
        $response = null;

        $location = $this->getDefaultDisk($location);

        if( $request->hasfile( $element ) )    {

            $cover = $request->file( $element );

            $response = Storage::disk( $location )->putFile( $type, new File($cover));
        }

        return $response;
    }

    protected function getDefaultDisk($disk = 's3')
    {
        return env('APP_ENV') === 'local' ? 'public' : $disk;
    }

    protected function getDefaultBucket()
    {
        $def_bucket = 'eservify-s3';

        if( config('app.env') == 'staging' )
            $def_bucket = 'eservify-dev';

        if( env('APP_ENV') == 'testing' )
            $def_bucket = 'eservify-test';

        return $def_bucket;
    }
}
