<?php
namespace App\Traits;

use Jenssegers\Optimus\Optimus;

trait Sluggable
{
    public static function bootSluggable()
    {
        static::creating(function ($model) {

            $model->slug = self::generateSlug($model);

            $model->slug = self::prefixSlug($model) . $model->slug . self::suffixSlug($model);

        });

        // static::updating(function ($model) {

        //     $model->slug = self::generateSlug($model);

        //     $model->slug = self::prefixSlug($model) . $model->slug . self::suffixSlug($model);

        // });
    }

    protected static function generateSlug($model)
    {
        return str_slug($model->name);
    }

    protected static function prefixSlug($model)
    {
        return '';
    }

    protected static function suffixSlug($model)
    {
        $prefix_slug = self::prefixSlug($model);

        $slug = $prefix_slug . $model->slug;

        $latestSlug =
        static::whereRaw("slug LIKE '%$slug%'")
            ->latest('id')
            ->value('slug');

        if ($latestSlug) {

            $pieces = explode('-', $latestSlug);

            $number = intval(end($pieces));

            return '-' . app(Optimus::class)->encode($number + 1);
        }
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
