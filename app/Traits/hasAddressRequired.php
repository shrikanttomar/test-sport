<?php

namespace App\Traits;

use App\Models\BrandCategory;

trait hasAddressRequired
{
    /**
     * check address required on the basis of brand,
     * category and product configuration
     *
     * @param App\\Models\\Brand $brand
     * @param App\\Models\\Category $category
     * @param App\\Models\\Product $product
     *
     * @return Boolean
     */
    protected function getAddressRequired($brand, $category, $product)
    {
        $required = false;

        if($product !== null)   {

            if($product->address_required == 1)
                $required = true;

        } else if($category !== null && $brand !== null)    {

            $bc = BrandCategory::where([
                'brand_id'      =>  $brand->id,
                'category_id'   =>  $category->id,
            ])->first();

            $required = $bc->is_address_required;
        }

        return $required;
    }
}
