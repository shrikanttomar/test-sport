<?php

namespace App\Traits;

use App\Models\User;

trait ViewNotifications
{
    public function getNotificationAttribute()
    {
        $n = collect(['items' => collect([]), 'read' => 0, 'unread' => 0, 'total' => 0]);

        $user = User::whereId($this->id)->first();

        if ($user) {
            foreach ($user->notifications as $notification) {
                $record = new \stdClass;
                $record->id             =   $notification->id;
                $record->cover          =   $notification->data['cover'];
                $record->title          =   $notification->data['title'];
                $record->excerpt        =   $notification->data['excerpt'];
                $record->description    =   $notification->data['description'];
                $record->read_at        =   $this->formattedDate($notification->read_at);
                $record->created_at     =   $this->formattedDate($notification->created_at);
                $record->updated_at     =   $this->formattedDate($notification->updated_at);

                $n['items']->push($record);
            }

            $n['read']      =   $user->readnotifications->count();
            $n['unread']    =   $user->unreadnotifications->count();
            $n['total']     =   $user->notifications->count();
        }

        return $n;
    }

    protected function formattedDate($value)
    {
        if ( $value )
            return $value->format('Y-m-d H:i:s');

        return $value;
    }
}
