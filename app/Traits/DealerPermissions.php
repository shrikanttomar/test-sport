<?php

namespace App\Traits;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;

trait DealerPermissions
{
    protected function hasPermissions(Order $order, $dealer_perm, $employee_perm)
    {
        $dealer = Auth::user();

        $dealer_location_ids = $this->getDealerLocationId($dealer, $dealer_perm);
        $employee_location_ids = $this->getEmployeeLocationId($dealer, $employee_perm);

        $order_ids = $this->getOrderId($dealer, $dealer_location_ids, $employee_location_ids);

        return in_array($order->id, $order_ids);
    }

    protected function getEmployeeLocationId($user, $permission)
    {
        return $user
                    ->permissions
                    ->where('slug', $permission)
                    ->pluck('pivot')
                    ->pluck('employer_location_id')
                    ->toArray();
    }

    protected function getDealerLocationId($user, $permission)
    {
        return $user
                    ->permissions
                    ->where('slug', $permission)
                    ->pluck('pivot')
                    ->pluck('employer_location_id')
                    ->toArray();
    }

    protected function getOrderId($user, $dealer_location_ids, $employee_location_ids)
    {
        $dealer_order_ids = Order::whereIn('dealer_location_id', $dealer_location_ids)
                                ->get()
                                ->pluck('id')
                                ->toArray();

        $employees_order_ids = Order::whereIn('dealer_location_id', $employee_location_ids)
                                ->where('created_by', $user->id)
                                ->get()
                                ->pluck('id')
                                ->toArray();

        return array_merge($dealer_order_ids, $employees_order_ids);
    }
}
