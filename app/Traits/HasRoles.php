<?php
namespace App\Traits;

trait HasRoles
{
    /**
     * Assign the given role to the user.
     *
     * @param string $role
     * @return mixed
     */
    public function assignRole($role)
    {
        return $this->roles()->save(Role::whereSlug($role)->firstOrFail());
    }
    /**
     * Determine if the user has the given role.
     *
     * @param mixed $role
     * @return boolean
     */
    public function hasRole( ... $roles)
    {
		foreach ($roles as $role) {

			if ($this->roles->contains('slug', $role)) {

				return true;
			}
        }

        return false;

        // if (is_string($roles)) {

        //     return $this->roles->contains('slug', $roles);
        // }

        // return !!$roles->intersect($this->roles)->count();
    }
}
