<?php
namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait WhoColumns
{
    public static function bootWhoColumns()
    {
        static::creating(function($table)  {

            $table->created_by = Auth::check() ? Auth::user()->id : 0;
        });

        static::created(function($table)  {

            $table->created_by = Auth::check() ? Auth::user()->id : 0;
        });

        static::updating(function($table)  {
            $table->updated_by = Auth::check() ? Auth::user()->id : 0;
        });

        static::deleting(function($table)  {
            $table->deleted_by = Auth::check() ? Auth::user()->id : 0;
            $table->save(); // Hack to save deleted by column
        });

        // TODO: Issue with restoring and restored event
        // static::restoring(function($table)  {
        //     $table->deleted_by = '';
        //     // $table->save(); // Hack to save deleted by column
        // });
    }

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updated_by_user()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function deleted_by_user()
    {
        return $this->belongsTo(User::class, 'deleted_by', 'id');
    }

    public function scopewithWho($query)
    {
        return $query->with(['created_by_user', 'updated_by_user', 'deleted_by_user']);
    }
}
