<?php

namespace App\Traits;

use App\Models\Order;
use App\Models\Product;

trait ViewCart
{
    public function getCartAttribute()
    {
        $cart_items = collect(['items' => collect([]), 'total_items' => 0, 'total_value' => 0]);

        if(! $this->isCustomer()) {

            return $cart_items;
        }

        if (\Cart::session($this->id)->isEmpty()) {

            $cart_items['items'][]    =   $this->emptyCart();
        }   else    {

            $items = \Cart::session($this->id)->getContent();

            $already_linked = [];
            foreach ($items as $item) {
                array_push($already_linked, $item->attributes->linked_to);
            }

            foreach ($items as $item) {

                // Get Orders
                $product = Product::active()
                                ->with(['digital_product'])
                                ->whereId($item->attributes->id)
                                ->first();

                $display_products = $product->digital_product->pluck('id')->toArray();

                $orders  = Order::active()
                                ->with(['detail.product'])
                                ->whereHas('detail', function ($query) use ($display_products) {
                                    $query->whereIn('product_id', $display_products);
                                })
                                ->where('user_id', $this->id)
                                ->whereNotIn('id', $already_linked)
                                ->where('status', '!=', 'Pending')
                                ->get();

                $cart_items['items'][]    =   [
                    'id'                =>  $item->id,
                    'name'              =>  $item->name,
                    'price'             =>  $item->price,
                    'quantity'          =>  $item->quantity,
                    'warranty_product'  =>  Product::active()->whereId($item->attributes->id)->first(),
                    'linked_order'      =>  Order::active()->whereId($item->attributes->linked_to)->first() ?? $this->defaultOrder(),
                    // 'orders'            =>  $orders->count() ? $orders : [$this->defaultOrder()]
                    'orders'            =>  $orders
                ];
            }

            $cart_items['total_items'] = \Cart::session($this->id)->getTotalQuantity();
            $cart_items['total_value'] = \Cart::session($this->id)->getTotal();
        }

        return $cart_items;
    }

    protected function defaultOrder()    {

        $record = new Order;

        $data = array_combine(
            $record->getFillable(),
            array_fill(0, count($record->getFillable()), null)
        );
        $record->fill($data);
        $record->id = 0;

        return $record;
    }

    protected function defaultProduct() {

        $record = new Product;

        $data = array_combine(
            $record->getFillable(),
            array_fill(0, count($record->getFillable()), null)
        );
        $record->fill($data);
        $record->id = 0;

        return $record;
    }

    protected function emptyCart()    {

        return [
            'id'                =>  0,
            'name'              =>  '',
            'price'             =>  0,
            'quantity'          =>  0,
            'warranty_product'  =>  $this->defaultProduct(),
            'linked_order'      =>  $this->defaultOrder(),
            'orders'            =>  [$this->defaultOrder()]
        ];
    }
}
