<?php

namespace App\Traits;

use Jenssegers\Optimus\Optimus;

trait HashId
{
    public function __constructor()
    {
        $this->appends['token'];
    }

    public function getTokenAttribute()
    {
        return app(Optimus::class)->encode($this->id);
    }

    public function getRouteKey()
    {
        return $this->token;
    }

    public function resolveRouteBinding($value)
    {
        $id = app(Optimus::class)->decode($value);

        return $this->where($this->getRouteKeyName(), $id)->first();
    }
}
