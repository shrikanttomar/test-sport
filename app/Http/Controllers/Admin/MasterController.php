<?php

namespace App\Http\Controllers\Admin;

use Basecode\Core\Controllers\Admin\BackendController;
use App\Classes\Repositories\MasterRepository as Repository;
use App\Classes\Permissions\MasterPermission as Permission;

class MasterController extends BackendController {
    
    public $repository, $permission;

    public function __construct( Repository $repository, Permission $permission ) {
        $this->repository = $repository;
        $this->permission = $permission;
        parent::__construct( $repository, $permission );
    }
}
