<?php

namespace App\Classes\Repositories;

use Basecode\Core\Repositories\Repository;
use App\Master;

class MasterRepository extends Repository {
    public $viewIndex = 'admin.masters.index';
    public $viewCreate = 'admin.masters.create';
    public $viewEdit = 'admin.masters.edit';
    public $viewShow = 'admin.masters.show';
    public $viewForm = 'admin.masters.form';

    public $routeIndex = 'admin.masters.index';
    public $routeCreate = 'admin.masters.create';
    public $routeStore = 'admin.masters.store';
    public $routeEdit = 'admin.masters.edit';
    public $routeUpdate = 'admin.masters.update';
    public $routeShow = 'admin.masters.show';
    public $routeDelete = 'admin.masters.destroy';

    public $storeValidateRules = [
        'title' => 'required|unique:masters,title'
    ];

    public $storeValidateRulesMsg = [
        
    ];

    public $updateValidateRules = [
        'title' => 'required|unique:masters,title'
    ];

    public $model = Master::class;
}