<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::group(['namespace' => 'Admin'], function() {
    
    Route::get('masters', ['as' => 'admin.masters.index', 'uses' => 'MasterController@index']);
    Route::get('masters/create', ['as' => 'admin.masters.create', 'uses' => 'MasterController@create']);
    Route::post('masters', ['as' => 'admin.masters.store', 'uses' => 'MasterController@store']);
    Route::get('masters/{id}/show', [ 'as' => 'admin.masters.show', 'uses' => 'MasterController@show' ]);
    Route::get('masters/{id}/edit', ['as' => 'admin.masters.edit', 'uses' => 'MasterController@edit']);
    Route::put('masters/{id}', ['as' => 'admin.masters.update', 'uses' => 'MasterController@update']);
    Route::delete('masters/{id}/destroy', ['as' => 'admin.masters.destroy', 'uses' => 'MasterController@destroy']);
    
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
